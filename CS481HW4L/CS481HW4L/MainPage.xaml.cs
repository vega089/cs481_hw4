﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using System.Collections.ObjectModel;



namespace CS481HW4L
{


    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            Causes();
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            Causes();
            ListViewImages.IsRefreshing = false;
        }

    


        public void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            customcell itemTapped = (customcell)listView.SelectedItem;
         
            Navigation.PushModalAsync(new descriptionPage(itemTapped.data, itemTapped.url));
        }
   


        private void Causes()
        {
            var imagesForListView = new ObservableCollection<customcell>()
        {

            new customcell ()
            {
                IconSource=ImageSource.FromFile("Homelessness.jpg"),
                Cause="Homelessness",
                url= "https://www.endhomelessness.org",
                Info="Issues and charities",
                data = "Your Support Drives Our Progress"+
                "The Alliance has accomplished much in pursuit of its mission. We have used research, educated the media, organized conferences, and worked at the local level to articulate a clear strategy for ending homelessness.\n Federal programs have been reoriented around this strategy, and funding levels have improved substantially with bipartisan support." +
                "\nLocal communities have learned from each other and as a result, have tailored strategies to make progress at the local level." + 
                "\nAs a result, the number of American’s experiencing homelessness has gone down.Between early 2010 and early 2016:"+
                "\n\tOverall homelessness dropped 13 percent(from 637, 000 to 550, 000)."+
                "\n\tIndividuals experiencing chronic homelessness went down by 27 percent(from 106, 100 to 77, 500)."+
                "\nVeterans experiencing homelessness declined from nearly 40 percent(from 65, 500 in 2011 to 39, 500)."+
                "\nFamilies with children experiencing homelessness declined from 79, 400 to 65, 000(18.1 percent)."+
                "\nWe are proud to have had a role in this progress.And your donation will help the Alliance continue this work."
                ,


            },

            new customcell ()
            {
                IconSource=ImageSource.FromFile("BreastCancer.jpg"),
                Cause="Breast Cancer",
                url= "https://www.bcrf.org",
                Info="Issues and charities",
                data = "Breast cancer research is a multi-billion-dollar industry. It makes up one of the largest research areas for federal National Cancer InstituteTrusted Source funding, with nearly $520 million spent in fiscal year 2016. Moreover," +
                " the Department of Defense’s Breast Cancer Research Program dedicates another $130 million to research annually." +
                "\nBut billions more come every year from the nonprofit sector, which raises an estimated $2.5 to $3.25 billion for breast cancer in a given fiscal year." +
                "\nThough breast cancer receives the most attention every October during Breast Cancer Awareness Month, charities and nonprofits work year - round to raise funds for the prevention, treatment, and cure of the disease."+
                "\nThey also provide much - needed support services for patients and caregivers.Donations are accepted at any time." +
                "\nYet it can be daunting for the average donor to figure out where a donation can have the most impact.Thanks to the ubiquity of pink ribbons, pink - packaged products, and special pink - bedazzled fundraising walks and events, " +
                "it can be hard to know where your charitable giving will have the most impact.\n" +
                "\n\n  If you are planning on donating, whether it’s a one - time pledge or setting up a recurring contribution, we’ve compiled a list of organizations to help get you started. ",

            },

              new customcell ()
            {
                IconSource=ImageSource.FromFile("animals.jpg"),
                Cause="Animal Humane",
                url= "https://www.awionline.org",
                Info="Issues and charities",
                data ="Every year Animal Humane Society cares for more than 22, 000 animals in need and helps thousands more through programs for people and pets."+
                "\n\n As one of the nation's leading animal welfare organizations, AHS is transforming the way shelters care for animals and engage their communities. "+
                "\nFrom innovative medical and behavior programs to investments in outreach and advocacy, we’re advancing animal welfare and creating a more humane world for animals everywhere."
              ,

            },

        };

            ListViewImages.ItemsSource = imagesForListView;




        }

        void Handle_Delete(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var listItem = (ObservableCollection<customcell>)ListViewImages.ItemsSource;

            customcell list = (customcell)menuItem.CommandParameter;
            listItem.Remove(list);
        }




    }



}